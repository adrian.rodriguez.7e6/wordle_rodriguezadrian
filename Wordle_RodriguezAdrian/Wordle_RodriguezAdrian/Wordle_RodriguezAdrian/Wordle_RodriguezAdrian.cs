﻿using System;
using System.IO;

namespace Wordle_RodriguezAdrian
{
    public class Wordle_RodriguezAdrian
    {
        /// <summary>
        /// Programa principal.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            string language = "Es";

            int option;
            string path = @"..\..\..\..\Ficheros";
            do
            {
                Console.Clear();
                if (File.Exists(path + @"\Menu\Titulo.txt"))
                {
                    using (StreamReader sr = File.OpenText(path + @"\Menu\Titulo.txt"))
                    {
                        string s = File.ReadAllText(path + @"\Menu\Titulo.txt");
                        Console.WriteLine(s);
                    }
                }
                else
                {
                    Console.WriteLine($"No existe el fichero en este directorio");
                }
                if (File.Exists(path + @"\Language\" + language + @"\Menu.txt"))
                {
                    using (StreamReader sr = File.OpenText(path + @"\Language\" + language + @"\Menu.txt"))
                    {
                        string s = File.ReadAllText(path + @"\Language\" + language + @"\Menu.txt");
                        Console.WriteLine(s);
                    }
                }
                else
                {
                    Console.WriteLine($"No existe el fichero en este directorio");
                }
                option = Convert.ToInt32(Console.ReadLine());
                switch (option)
                {
                    case 1:
                        StartGame(language, path);
                        break;
                    case 2:
                        Instrucciones(path, language);
                        break;
                    case 3:
                        Historial(path);
                        break;
                    case 4:
                        language = Language(language);
                        break;
                    case 5:
                        break;
                    default:
                        Console.WriteLine("\nOpción incorrecta!\n");
                        Console.ReadLine();
                        break;
                }
            } while (option != 5);
        }

        /// <summary>
        /// Funcion que se utiliza para seleccionar el idioma.
        /// </summary>
        /// <param name="language"></param>
        /// <returns>String del idioma seleccionado.</returns>
        public static string Language(string language)
        {
            int option;
            do
            {
                Console.Clear();
                if (language == "Es")
                {
                    Console.WriteLine("¿Que idioma quieres escoger?");
                }
                if (language == "Cat")
                {
                    Console.WriteLine("Que idioma vols triar?");
                }
                Console.WriteLine(" 0 - Exit\n 1 - Es\n 2 - Cat\n");
                option = Convert.ToInt32(Console.ReadLine());
                switch (option)
                {
                    case 0:
                        break;
                    case 1:
                        language = "Es";
                        break;
                    case 2:
                        language = "Cat";
                        break;
                    default:
                        Console.WriteLine("\nOpción incorrecta!\n");
                        break;
                }
            } while (option == default);
            return language;
        }

        /// <summary>
        /// Funcion que muestra el historial de las partidas realizadas.
        /// </summary>
        /// <param name="path"></param>
        static void Historial(string path)
        {
            if (File.Exists(path + @"\Historial\Historial.txt"))
            {
                using (StreamReader sr = File.OpenText(path + @"\Historial\Historial.txt"))
                {
                    Console.Clear();
                    string s = File.ReadAllText(path + @"\Historial\Historial.txt");
                    Console.WriteLine(s);
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine($"No existe el fichero en este directorio");
            }
        }

        /// <summary>
        /// Funcion que muestra las instrucciones en su respectivo idioma.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="language"></param>
        static void Instrucciones(string path, string language)
        {
            if (File.Exists(path + @"\Language\" + language + @"\Instrucciones.txt"))
            {
                using (StreamReader sr = File.OpenText(path + @"\Language\" + language + @"\Instrucciones.txt"))
                {
                    Console.Clear();
                    string s = File.ReadAllText(path + @"\Language\" + language + @"\Instrucciones.txt");
                    Console.WriteLine(s);
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine($"No existe el fichero en este directorio");
            }
        }

        /// <summary>
        /// Codigo del juego principal.
        /// </summary>
        /// <param name="language"></param>
        /// <param name="path"></param>
        static void StartGame(string language, string path)
        {
            bool win = false;
            string palUsur = "";
            string palWordle = "";
            int vidas = 5;
            int lineaMatriz = 0;

            char[,] matriz = new char[5, 5];
            char[,] matrizColores = new char[5, 5];
            Console.Clear();
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    matriz[i, j] = '_';
                    Console.Write(matriz[i, j] + " ");
                }
                Console.WriteLine("");
            }

            palWordle = TakeSecretWord(path, language);

            do
            {
                palUsur = PedirPalabra(palUsur, language);
                Console.Clear();
                CompareWords(palUsur, ref vidas, palWordle, ref lineaMatriz, ref matriz, ref matrizColores, ref win);
            } while (vidas > 0 && win == false );

            AddMatch(path, language, win, palWordle, vidas);

        }

        /// <summary>
        /// Funcion que escoge de una forma random una palabra del fichero de palabras del respectivo idioma que estes jugando.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="language"></param>
        /// <returns>Un string con la palabra secreta.</returns>
        static string TakeSecretWord(string path, string language)
        {
            string palWordle = "";
            if (File.Exists(path + @"\Language\" + language + @"\Palabras.txt"))
            {
                using (StreamReader sr = File.OpenText(path + @"\Language\" + language + @"\Palabras.txt"))
                {
                    Random rnd = new Random();
                    int i = rnd.Next(0, 101);
                    palWordle = File.ReadAllLines(path + @"\Language\" + language + @"\Palabras.txt")[i];
                    //Console.WriteLine(palWordle);//BORRAR
                }
            }
            else
            {
                Console.WriteLine($"No existe el fichero en este directorio");
            }
            return palWordle;
        }

        /// <summary>
        /// Funcion que pide una palabra al usuario.
        /// </summary>
        /// <param name="palUsur"></param>
        /// <param name="language"></param>
        /// <returns>Un string con la palabra del usuario.</returns>
        static string PedirPalabra(string palUsur, string language)
        {
            do
            {
                if (language == "Es")
                {
                    Console.WriteLine("Introduce una palabra que contenga 5 letras.");
                }
                if (language == "Cat")
                {
                    Console.WriteLine("\r\nIntrodueix una paraula que contingui 5 lletres.");
                }
                palUsur = Console.ReadLine();
                
                if (CheckedIf5(palUsur))
                {
                    if (language == "Es")
                    {
                        Console.WriteLine("La palabra que has escrito no tiene 5 caracteres. Vuelve a intentarlo.");
                    }
                    if (language == "Cat")
                    {
                        Console.WriteLine("\r\nLa paraula que has escrit no té 5 caràcters. Torna a intentar-ho.");
                    }
                }
            } while (palUsur.Length != 5);
            return palUsur;
        }

        /// <summary>
        /// Funcion que mira si la palabra del usuario tiene 5 letras.
        /// </summary>
        /// <param name="palUsur"></param>
        /// <returns>Un bool true o false.</returns>
        public static bool CheckedIf5(string palUsur)
        {
            if (palUsur.Length != 5)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Funcion que compara si la palabra del usuario y la poalabra secreta son iguales.
        /// </summary>
        /// <param name="palUsur"></param>
        /// <param name="vidas"></param>
        /// <param name="palWordle"></param>
        /// <param name="lineaMatriz"></param>
        /// <param name="matriz"></param>
        /// <param name="matrizColores"></param>
        /// <param name="win"></param>
        static void CompareWords(string palUsur, ref int vidas, string palWordle, ref int lineaMatriz, ref char[,] matriz, ref char[,] matrizColores, ref bool win)
        {
            if (CheckedIfWin(palUsur, palWordle))
            {
                IfWin(palWordle);
                win = true;
            }
            else
            {
                vidas--;

                if (CheckedIfLose(vidas))
                {
                    IfLose(palWordle);
                }
                else
                {
                    palUsur = palUsur.ToUpper();
                    palWordle = palWordle.ToUpper();

                    char[] charPalUsur = palUsur.ToCharArray();
                    char[] charPalWordle = palWordle.ToCharArray();


                    for (int i = 0; i < 5; i++)
                    {
                        matriz[lineaMatriz, i] = charPalUsur[i];
                    }

                    CompararLetras(charPalUsur, charPalWordle, lineaMatriz, matrizColores);

                    PrintMatrizColoreada(matrizColores, matriz);
                    
                    lineaMatriz++;
                }
            }
        }

        /// <summary>
        /// Funcion que comprueba si pierdes.
        /// </summary>
        /// <param name="vidas"></param>
        /// <returns>Un bool true o false.</returns>
        public static bool CheckedIfLose(int vidas)
        {
            if (vidas == 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Funcion que comprueba si ganas.
        /// </summary>
        /// <param name="palUsur"></param>
        /// <param name="palWordle"></param>
        /// <returns>Un bool true o false.</returns>
        public static bool CheckedIfWin(string palUsur, string palWordle)
        {
            if (palUsur.ToUpper() == palWordle.ToUpper())
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Funcion que imprime texto por pantalla cuando ganas.
        /// </summary>
        /// <param name="palWordle"></param>
        static void IfWin(string palWordle)
        {
            Console.Clear();
            Console.WriteLine("Enhorabuena!!!\nHas adivinado la palabra.\nHas ganado!!!");
            Console.Write("La palabra era ");
            Console.BackgroundColor = ConsoleColor.Green;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write(palWordle);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("");
            Console.ReadLine();
        }

        /// <summary>
        /// Funcion que imprime texto por pantalla cuando pierdes.
        /// </summary>
        /// <param name="palWordle"></param>
        static void IfLose(string palWordle)
        {
            Console.Clear();
            Console.WriteLine("Vaya, has perdido :(\nTe has quedado sin vidas.");
            Console.Write("La palabra era ");
            Console.BackgroundColor = ConsoleColor.Red;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write(palWordle);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("");
            Console.ReadLine();
        }

        /// <summary>
        /// Funcion que compara las letras de la palabra del usuario.
        /// </summary>
        /// <param name="charPalUsur"></param>
        /// <param name="charPalWordle"></param>
        /// <param name="lineaMatriz"></param>
        /// <param name="matrizColores"></param>
        /// <returns>Una matriz con los colores que hay que ponerle a cada letra</returns>
        static char[,] CompararLetras(char[] charPalUsur, char[] charPalWordle, int lineaMatriz, char[,] matrizColores)
        {
            CompareYellow(charPalUsur, charPalWordle, lineaMatriz, matrizColores);
            CompareGreen(charPalUsur, charPalWordle, lineaMatriz, matrizColores);
            return matrizColores;
        }

        /// <summary>
        /// Funcion que compara las letras que estan en la palabra del usuario pero en diferente posicion y guarda en la matriz cuales hay que pintar en amarillo.
        /// </summary>
        /// <param name="charPalUsur"></param>
        /// <param name="charPalWordle"></param>
        /// <param name="lineaMatriz"></param>
        /// <param name="matrizColores"></param>
        /// <returns>Una matriz con los colores que hay que ponerle a cada letra</returns>
        static char[,] CompareYellow(char[] charPalUsur, char[] charPalWordle, int lineaMatriz, char[,] matrizColores)
        {
            for (int iPalUsur = 0; iPalUsur < 5; iPalUsur++)
            {
                for (int iPalWordle = 0; iPalWordle < 5; iPalWordle++)
                    if (charPalUsur[iPalUsur] == charPalWordle[iPalWordle])
                    {
                        matrizColores[lineaMatriz, iPalUsur] = 'y';
                    }
            }
            return matrizColores;
        }

        /// <summary>
        /// Funcion que compara las letras que estan en la palabra del usuario y en la misma posicion y guarda en la matriz cuales hay que pintar en verde.
        /// </summary>
        /// <param name="charPalUsur"></param>
        /// <param name="charPalWordle"></param>
        /// <param name="lineaMatriz"></param>
        /// <param name="matrizColores"></param>
        /// <returns>Una matriz con los colores que hay que ponerle a cada letra</returns>
        static char[,] CompareGreen(char[] charPalUsur, char[] charPalWordle, int lineaMatriz, char[,] matrizColores)
        {
            for (int i = 0; i < 5; i++)
            {
                if (charPalUsur[i] == charPalWordle[i])
                {
                    matrizColores[lineaMatriz, i] = 'g';
                }
            }
            return matrizColores;
        }

        /// <summary>
        /// Funcion que imprime el tablero de juego con los colores.
        /// </summary>
        /// <param name="matrizColores"></param>
        /// <param name="matriz"></param>
        static void PrintMatrizColoreada(char[,] matrizColores, char[,] matriz)
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (matrizColores[i, j] == 'g')
                    {
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write(matriz[i, j] + " ");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else if (matrizColores[i, j] == 'y')
                    {
                        Console.BackgroundColor = ConsoleColor.Yellow;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write(matriz[i, j] + " ");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        Console.Write(matriz[i, j] + " ");
                    }
                }
                Console.WriteLine("");
            }
        }

        /// <summary>
        /// Funcion que añade la partida al historial de partidas.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="language"></param>
        /// <param name="win"></param>
        /// <param name="palWordle"></param>
        /// <param name="vidas"></param>
        static void AddMatch(string path, string language, bool win, string palWordle, int vidas)
        {
            int intentos = 5 - vidas;
            if (language == "Es")
            {
                Console.WriteLine("¿Nombre del jugador?");
            }
            if (language == "Cat")
            {
                Console.WriteLine("Nom del jugador?");
            }
            string name = Console.ReadLine();
            if (File.Exists(path + @"\Historial\Historial.txt"))
            {
                using (StreamWriter sw = File.AppendText(path + @"\Historial\Historial.txt"))
                {
                    if (win==true)
                    {
                        sw.WriteLine("VICTORIA");
                    }
                    else
                    {
                        sw.WriteLine("DERROTA");
                    }
                    sw.WriteLine($"Name: {name}");
                    sw.WriteLine($"Secret Words: {palWordle}");
                    sw.WriteLine($"Lives used: {intentos}");
                    sw.WriteLine("=============================");
                }
            }
            else
            {
                Console.WriteLine($"No existe el fichero en este directorio");
            }
        }
    }
}
