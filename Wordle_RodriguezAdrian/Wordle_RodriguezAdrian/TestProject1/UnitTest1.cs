using Microsoft.VisualStudio.TestPlatform.TestHost;
using NUnit.Framework;
using Wordle_RodriguezAdrian;

namespace TestProject1
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }
        //Al haber hecho el wordle con el tipo de funciones y mentalidad que he escogido no puedo hacer el Unit test.

        [Test]
        public static void TestCheckedIfNot5()
        {
            Assert.AreEqual(true, Wordle_RodriguezAdrian.Wordle_RodriguezAdrian.CheckedIf5("maestro"));
        }

        [Test]
        public static void TestCheckedIf5()
        {
            Assert.AreEqual(false, Wordle_RodriguezAdrian.Wordle_RodriguezAdrian.CheckedIf5("lapiz"));
        }

        [Test]
        public static void TestCheckedIfNotWin()
        {
            Assert.AreEqual(false, Wordle_RodriguezAdrian.Wordle_RodriguezAdrian.CheckedIfWin("maestro","lapiz"));
        }

        [Test]
        public static void TestCheckedIfWin()
        {
            Assert.AreEqual(true, Wordle_RodriguezAdrian.Wordle_RodriguezAdrian.CheckedIfWin("lapiz", "lapiz"));
        }

        [Test]
        public static void TestCheckedIfNotLose()
        {
            Assert.AreEqual(false, Wordle_RodriguezAdrian.Wordle_RodriguezAdrian.CheckedIfLose(5));
        }

        [Test]
        public static void TestCheckedIfLose()
        {
            Assert.AreEqual(true, Wordle_RodriguezAdrian.Wordle_RodriguezAdrian.CheckedIfLose(0));
        }

        [Test]
        public static void TestCheckedIfNotLoseNegative()
        {
            Assert.AreEqual(false, Wordle_RodriguezAdrian.Wordle_RodriguezAdrian.CheckedIfLose(-2));
        }
    }
}